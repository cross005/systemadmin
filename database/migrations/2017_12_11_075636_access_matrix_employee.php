<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AccessMatrixEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_matrix_employee', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->string('system_id');
            $table->string('hasAccess');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('access_matrix_employee');
    }
}
