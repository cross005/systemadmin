<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = 'itsupport';
        $passwordHash = bcrypt($password);
        DB::table('Users')->insert([
            [
            'employee_id' => 550,
            'email' => 'kristine.delacruz@solarphilippines.ph',
            'remember_token' => '',
            ],

            [
            'employee_id' => 551,
            'email' => 'anton.panghulan@solarphilippines.ph',
            'remember_token' => '',
            ],


            [
            'employee_id' => 552,
            'email' => 'ralph.vitto@solarphilippines.ph',
            'remember_token' => '',
            ],


            [
            'employee_id' => 553,
            'email' => 'ivann.turla@solarphilippines.ph',
            'remember_token' => '',
            ],

            [
            'employee_id' => 554,
            'email' => 'jino.marchadesch@solarphilippines.ph',
            'remember_token' => '',
            ],


            [
            'employee_id' => 555,
            'email' => 'rein.guillermo@solarphilippines.ph',
            'remember_token' => '',
            ],
        ]);
    }
}
