<?php

use Illuminate\Database\Seeder;

class SystemDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('system_details')->insert([
            [
            'system_code' => 'hris',
            'name' => 'HRIS',
            'url' => '192.168.0.253',
            'status' => 'Active',
            ],
            [
            'system_code' => 'service_desk',
            'name' => 'Service Desk',
            'url' => '192.168.0.253',
            'status' => 'Active',
            ],
            [
            'system_code' => 'purchase_order',
            'name' => 'Purchase Order',
            'url' => '192.168.0.252',
            'status' => 'Active',
            ],
            [
            'system_code' => 'system_admin',
            'name' => 'System Admin',
            'url' => '192.168.0.251',
            'status' => 'Active',
            ],
        ]);
    }
}
