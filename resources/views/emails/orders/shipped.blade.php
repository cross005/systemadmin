@component('mail::message')
# {{ $content['title'] }}

Thank you accessing IT Enterprise Application/s. To proceed, please login using the following credentials:

<b>{{ $content['body'] }}</b>

<b>{{ $abc = $content['details'] }}</b>

Please note that you can modify your password via <b>Forgot Password</b> feature. Also, abovementioned credentials can be used to login <b>across all systems</b> developed by the IT Enterprise Application Team (e.i. Service Desk, HRIS, PRPO Automation, Material Requisition Tool).
{{ config('app.name') }}
@endcomponent