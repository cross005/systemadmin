<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'asdadsa') }}</title>

    <!-- Styles -->
    <link rel="shortcut icon" href="{{{ asset('solarphil.png') }}}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/b-1.4.2/b-html5-1.4.2/b-print-1.4.2/datatables.min.css"/>
 
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/b-1.4.2/b-html5-1.4.2/b-print-1.4.2/datatables.min.js"></script>
    <style type="text/css">
        th { font-size: 13px; }
        td { font-size: 13px; }  
    </style>
    
    @yield('scripts')
    </head>
<body>

    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a class="nav" href="{{ url('dashboard/buyer') }}"><img class="navbar-nav" width="100px" src="{{asset('solarpng.png')}}"></a>
                        </li>
                        <li>
                        <!-- Branding Image -->

                        <a class="navbar-brand" href="{{ url('dashboard/buyer') }}">{{ config('app.name', 'Laravel') }}</a>

                        </li>
                

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-center">
                            <li class="dropdown">
                                <div class="dropbtn">Purchase Request
                                    <div class="dropdown-content">
                                        <a href="/purchase_request/buyer/">Requests</a>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown">
                                <div class="dropbtn">Setup
                                    <div class="dropdown-content">
                                        <a href="/setup/project_profile/">Project Profile</a>
                                        <a href="/setup/user_accounts/">User Accounts</a>
                                        <a href="/setup/email_notification/">Email Notification</a>
                                        <a href="/setup/audit_trail/">Audit Trail</a>
                                    </div>
                                </div>
                            </li>
                            
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest());
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->full_name }}<span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="/purchase_request/profile/buyer/index">Profile</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')

    </div>
</body>
</html>