@extends('layouts.app')
    @section('scripts')
    <script type="text/javascript">
    $(function() {
        $('#example').DataTable({
            processing: true,
            //serverSide: true,
            //dom: 'Bfrtip',
            buttons: [
                { extend: 'excel', text: 'Export', 
                  exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8]
                  }}
            ],
            ajax: '/cms/audit_trail/getAuditTrailDatatables',
            columns: [
                {data: 'id', width: '100px'},
                {data: 'description', width: '100px'},

            ],

        });
    });
    </script>
    @endsection
    @section('content')
        <div class="container">
          <div class="row">
            <div class="col-md-12">
                @if(session()->has('success'))
                <div class="alert alert-success">
                        {{ session()->get('success') }}
                </div>
                @endif
              <div class="panel panel-default">
                <div class="panel-heading">Login Details</b></div>
                    <div class="panel-body orange">
                      <table id="example" class="display" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <td>Email</td>
                              <td>Status</td>

                            </tr>
                          </thead>
                      </table>
                    </div>
              </div>
            </div>
          </div>
      </div>


    <script type="text/javascript">



    </script>



    @endsection
    <!-- toastr notifications -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!-- jQuery -->
    {{-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script> --}}
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

    <!-- Bootstrap JavaScript -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

    <!-- toastr notifications -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
</body>
</html>

