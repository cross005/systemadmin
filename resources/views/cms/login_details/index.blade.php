@extends('layouts.app')
    @section('scripts')
    <script type="text/javascript">
    $(function() {
        $('#example').DataTable({
            processing: true,
            //serverSide: true,
            //dom: 'Bfrtip',
            buttons: [
                { extend: 'excel', text: 'Export', 
                  exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8]
                  }}
            ],
            ajax: '/cms/login_details/getLoginDetailsDatatables',
            columns: [
                {data: 'email', width: '100px'},
                {data: 'status', width: '100px'},
                {sortable: false,
                 "render": function ( data, type, full, meta ) {
                 	var id = full.id;
                 	var status = full.status;

                    if(status == 'Deactivate'){
                        return '<a href="#" class="activate-modal" data-id='+id+'><button class="btn btn-success" type="button">Activate</button></a>';
                    }

                    return '<a href="#" class="deactivate-modal" data-id='+id+'><button class="btn btn-default" type="button">Deactivate</button></a>';
                 },
                 width: '100px'
                },
            ],

        });
    });
    </script>
    @endsection
    @section('content')
        <div class="container">
          <div class="row">
            <div class="col-md-12">
                @if(session()->has('success'))
                <div class="alert alert-success">
                        {{ session()->get('success') }}
                </div>
                @endif
              <div class="panel panel-default">
                <div class="panel-heading">Login Details</b></div>
                    <div class="panel-body orange">
                      <table id="example" class="display" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <td>Email</td>
                              <td>Status</td>
                              <td>Action</td>
                            </tr>
                          </thead>
                      </table>
                    </div>
              </div>
            </div>
          </div>
      </div>


    <!-- Modal for deactivate users-->
    <div id="deactivateModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                	<input type="hidden" class="form-control" id="pr_id" disabled>
                    <h3 class="text-center">Are you sure you want to deactivate this account?</h3>
                    <br />
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success deactivate" data-dismiss="modal">
                            Submit
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Modal for activate users-->
    <div id="activateModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                	<input type="hidden" class="form-control" id="pr_id" disabled>
                    <h3 class="text-center">Are you sure you want to activate this account?</h3>
                    <br />
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success activate" data-dismiss="modal">
                            Submit
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    	
 		//Deactivation of users
        $(document).on('click', '.deactivate-modal', function() {
            $('#pr_id').val($(this).data('id'));
            $('#deactivateModal').modal('show');
            id = $('#pr_id').val();
        });
        $('.modal-footer').on('click', '.deactivate', function() {
            $.ajax({
                type: 'get',
                url: '/cms/login_details/deactivateAccount/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {
                  window.location.href = "";
                }
            });
        });


        //Activation of users
        $(document).on('click', '.activate-modal', function() {
            $('#pr_id').val($(this).data('id'));
            $('#activateModal').modal('show');
            id = $('#pr_id').val();
        });
        $('.modal-footer').on('click', '.activate', function() {
            $.ajax({
                type: 'get',
                url: '/cms/login_details/activateAccount/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {
                  window.location.href = "";
                }
            });
        });




    </script>



    @endsection
    <!-- toastr notifications -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!-- jQuery -->
    {{-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script> --}}
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

    <!-- Bootstrap JavaScript -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

    <!-- toastr notifications -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
</body>
</html>

