@extends('layouts.app')
    @section('scripts')
    @endsection
    @section('content')
        <div class="container">
          <div class="row">
            <div class="col-md-12">
                @if(session()->has('success'))
                <div class="alert alert-success">
                        {{ session()->get('success') }}
                </div>
                @endif
              <div class="panel panel-default">
                <div class="panel-heading">Access Matrix - <b>Department</b></div>
                    <div class="panel-body orange">
                    @if(session()->has('matrixSuccess'))
                    <div class="alert alert-success">
                            {{ session()->get('matrixSuccess') }}
                    </div>
                    @endif
                      <table id="example" class="display" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Department</th>
                              <th>HRIS</th>
                              <th>Service Desk</th>
                              <th>Purchase Order</th>
                              <th>System Admin</th>
                            </tr>
                          </thead>
                          <tbody>
                                  {!! Form::open(['action' => 'AccessMatrixController@store', 'method' => 'POST', 'id'=>'matrixForm']) !!}
                                    @foreach($accessMatrix as $accessMatrixs)
                                    <tr>
                                        <td>{{$accessMatrixs->department_code}}</td>
                                        <td>{{ Form::checkbox('hris'.$accessMatrixs->id, null, $accessMatrixs->hris == 1 ? 'true':'', ['class' => 'hris']) }}</td>
                                        <td>{{ Form::checkbox('service_desk'.$accessMatrixs->id, null, $accessMatrixs->service_desk == 1 ? 'true':'', ['class' => 'service_desk']) }}</td>
                                        <td>{{ Form::checkbox('purchase_order'.$accessMatrixs->id, null, $accessMatrixs->purchase_order == 1 ? 'true':'', ['class' => 'purchase_order']) }}</td>
                                        <td>{{ Form::checkbox('system_admin'.$accessMatrixs->id, null, $accessMatrixs->system_admin == 1 ? 'true':'', ['class' => 'system_admin']) }}</td>
                                    </tr>
                                    @endforeach


                          </tbody>
                      </table>
                    </div>

                  </div>

                    <div class="pull-right">
                        {{Form::submit('Save Changes', ['class' => 'btn btn-primary', 'id'=>'matrixSave'])}}
                        {{Form::button('Cancel', ['class' => 'btn btn-default', 'id'=>'cancelBtn'])}}
                    </div>
                    {!! Form::close() !!}

            </div>
          </div>
        </div>


    @endsection
    <!-- toastr notifications -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!-- jQuery -->
    {{-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script> --}}
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

    <!-- Bootstrap JavaScript -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

    <!-- toastr notifications -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
</body>
</html>

