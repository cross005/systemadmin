@extends('layouts.app')
    @section('content')
        <div class="container">
          <div class="row">
            <div class="col-md-12">
                @if(session()->has('success'))
                <div class="alert alert-success">
                        {{ session()->get('success') }}
                </div>
                @endif
              <div class="panel panel-default">
                <div class="panel-heading">Systems 
                </div>


                	<div class="panel-body orange text-center">
                    {!! Form::open(['action' => ['SystemDetailsController@update', $systemDetails->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}

                        <div class="row pink">
                            <div class="form-group col-md-6 red">
                                {{Form::label('pr_no', 'System name', ['class' => 'col-md-3 blue'])}}
                                <span class="col-md-6 yellow">{{Form::text('system_name', $systemDetails->name, ['class' => 'form-control col-md-2','id' => 'pr_no'])}}</span>
                                <span class="help-block">
                                    <strong>{{ $errors->first('system_name') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="row pink">
                            <div class="form-group col-md-6 red">
                                {{Form::label('pr_no', 'URL', ['class' => 'col-md-3 blue'])}}
                                <span class="col-md-6 yellow">{{Form::text('url', $systemDetails->url, ['class' => 'form-control col-md-2','id' => 'pr_no'])}}</span>
                                <span class="help-block">
                                    <strong>{{ $errors->first('url') }}</strong>
                                </span>
                            </div>
                        </div>               
             

                    
						<div class="row pink">
                            <div class="form-group col-md-6 red">
					          {{Form::button('Submit', ['class' => 'btn btn-success btnModal', 'data-toggle' => 'modal', 'data-target' => '#myModal'])}}
					          {{Form::button('Cancel', ['class' => 'btn btn-danger', 'data-toggle' => 'modal', 'data-target' => '#cancelModal'])}}
					         </div>
					    </div>			
                    </div>
              </div>
            </div>
          </div>

		  <!-- Create system Modal -->
		  <div class="modal fade" id="myModal" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Proceed with Creation</h4>
		        </div>
		        <div class="modal-body">
		            <p>Do you want to proceed for the creation of System Details?</p>
		        </div>
		        <div class="modal-footer">
		          {{Form::submit('Submit', ['class' => 'btn btn-success'])}}
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		      
		    </div>
		  </div>


		  <!-- Modal for cancellation of system details-->
		  <div class="modal fade" id="cancelModal" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Cancel</h4>
		        </div>
		        <div class="modal-body">
		            <p>Do you want to proceed for the cancellation of System Details?</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-danger cancel" data-href="{{URL::to('system_details/')}}">
		                Cancel
		          </button>
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		      
		    </div>
		  </div>
      </div>
      {!! Form::close() !!}


	<script type="text/javascript">
		//Cancellation of system details
		$('.modal-footer').on('click', '.cancel', function() {
		    window.location.href = $(this).data('href');
	});
	</script>


	
     @endsection
    <!-- toastr notifications -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!-- jQuery -->
    {{-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script> --}}
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

    <!-- Bootstrap JavaScript -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

    <!-- toastr notifications -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
</body>
</html>

