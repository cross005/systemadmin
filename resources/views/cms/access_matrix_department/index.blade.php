@extends('layouts.app')
    @section('scripts')

    @endsection
    @section('content')
    <style type="text/css">
    
    .tblLineHeight{
        margin-bottom: 5px;
    }

    input[type=checkbox]{
         /* Double-sized Checkboxes */
  -ms-transform: scale(1); /* IE */
  -moz-transform: scale(1); /* FF */
  -webkit-transform: scale(1); /* Safari and Chrome */
  -o-transform: scale(1); /* Opera */
  margin-bottom: 5px;
    }
    

    </style>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
                @if(session()->has('matrixSuccess'))
                <div class="alert alert-success">
                        {{ session()->get('matrixSuccess') }}
                </div>
                @endif
              <div class="panel panel-default">
                <div class="panel-heading">Access Matrix</b></div>
                    <div class="panel-body orange">
                        <div class="row">
                            <div class="col-md-2">
                                <b>Department</b>

                                <br />
                                @foreach($accessMatrix as $accessMatrixs)
                                    <span class="tblLineHeight">{{$accessMatrixs->department_id}}</span>
                                <br />
                                 @endforeach
                            </div>

                            
                            <div class="col-md-10">

                            <table id="example" class="display" border="0" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                    @foreach($systemDetails as $systemDetail)
                                        <th>{{$systemDetail->name}}</th>
                                    @endforeach
                                    </tr>
                                </thead>

                                    {!! Form::open(['action' => 'AccessMatrixDepartmentController@store', 'method' => 'POST', 'id'=>'matrixForm']) !!}


                                    

                                <tbody>
                                    <tr>
                                        @for($i = 0; $i < $accessMatrixAllCount; $i++)
                                            @if($i%$systemDetailsCount == 0)
                                                        <tr></tr>
                                            @endif
                                            <td class="tdLineHeight">{{ Form::checkbox('hris'.$accessMatrixAll[$i]->id, $accessMatrixAll[$i]->id, $accessMatrixAll[$i]->hasAccess == 1 ? 'true':'', ['class' => 'checkboxField']) }} {{$accessMatrixAll[$i]->id}} </td>
                                        @endfor
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
                
            </div>
            {{Form::submit('Save Changes', ['class' => 'btn btn-primary', 'id'=>'matrixSave'])}}
        </div>
    </div>
                        



    <!-- Modal for deactivate users-->
    <div id="deactivateModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                	<input type="hidden" class="form-control" id="pr_id" disabled>
                    <h3 class="text-center">Are you sure you want to deactivate this account?</h3>
                    <br />
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success deactivate" data-dismiss="modal">
                            Submit
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Modal for activate users-->
    <div id="activateModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                	<input type="hidden" class="form-control" id="pr_id" disabled>
                    <h3 class="text-center">Are you sure you want to activate this account?</h3>
                    <br />
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success activate" data-dismiss="modal">
                            Submit
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
{!! Form::close() !!}
    <script type="text/javascript">

 		//Deactivation of users
        $(document).on('click', '.deactivate-modal', function() {
            $('#pr_id').val($(this).data('id'));
            $('#deactivateModal').modal('show');
            id = $('#pr_id').val();
        });
        $('.modal-footer').on('click', '.deactivate', function() {
            $.ajax({
                type: 'get',
                url: '/cms/login_details/deactivateAccount/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {
                  window.location.href = "";
                }
            });
        });


        //Activation of users
        $(document).on('click', '.activate-modal', function() {
            $('#pr_id').val($(this).data('id'));
            $('#activateModal').modal('show');
            id = $('#pr_id').val();
        });
        $('.modal-footer').on('click', '.activate', function() {
            $.ajax({
                type: 'get',
                url: '/cms/login_details/activateAccount/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {
                  window.location.href = "";
                }
            });
        });




    </script>



    @endsection
    <!-- toastr notifications -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!-- jQuery -->
    {{-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script> --}}
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

    <!-- Bootstrap JavaScript -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

    <!-- toastr notifications -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
</body>
</html>

