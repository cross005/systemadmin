<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//CRUD FOR SYSTEM DETAILS
//Admin Setup
Route::resource('system_details', 'SystemDetailsController');
Route::get('/cms/system_details/getSystemDetailsDatatables', 'SystemDetailsController@getSystemDetailsDatatables');
//Deactivate users account
Route::get('/cms/system_details/deactivateSystemDetails/{id}', 'SystemDetailsController@deactivateSystemDetails');
//Activate users account
Route::get('/cms/system_details/activateSystemDetails/{id}', 'SystemDetailsController@activateSystemDetails');


//CRUD FOR LOGIN DETAILS
//Admin Setup
Route::resource('login_details/', 'LoginDetailsController');
Route::get('/cms/login_details/getLoginDetailsDatatables', 'LoginDetailsController@getLoginDetailsDatatables');
//Deactivate users account
Route::get('/cms/login_details/deactivateAccount/{id}', 'LoginDetailsController@deactivateAccount');
//Activate users account
Route::get('/cms/login_details/activateAccount/{id}', 'LoginDetailsController@activateAccount');



//CRUD FOR ACCESS MATRIX
//Admin Setup
Route::resource('access_matrix', 'AccessMatrixController');
Route::get('/access_matrix/employee', 'AccessMatrixController@getEmployeeRecord');

Route::get('/cms/access_matrix/getAccessMatrixDatatables', 'AccessMatrixController@getAccessMatrixDatatables');


//CRUD FOR ACCESS MATRIX
//Admin Setup
Route::resource('access_matrix_department', 'AccessMatrixDepartmentController');
Route::get('/access_matrix_department/employee', 'AccessMatrixDepartmentController@getEmployeeRecord');

Route::get('/cms/access_matrix_department/getAccessMatrixDatatables', 'AccessMatrixDepartmentController@getAccessMatrixDatatables');



//CRUD FOR AUDIT TRAIL
//Admin Setup
Route::resource('audit_trail', 'AuditTrailController');
Route::get('/cms/audit_trail/getAuditTrailDatatables', 'AuditTrailController@getAuditTrailDatatables');




//Previewing Mailables In The Browser
Route::get('/mailable', function () {
    $user = App\User::find(1);

    return new App\Mail\SendPasswordMailable($user);
});



Auth::routes();
Route::get('/verifyLogin', 'Auth\RegisterController@showverifyLogin');
//Route::get('/system_details', 'HomeController@index')->name('home');
Route::get('auth.verifyLogin', 'PagesController@createRecord');
Route::get('/home', 'HomeController@index')->name('home');


//ERROR FOUND
//Not Authorize
Route::get('errors/not_authorized', 'PagesController@not_authorized');
//No department found
Route::get('errors/no_department', 'PagesController@no_department');
