<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

use Yajra\DataTables\Facades\DataTables;

use Response;
use Session;
use DB;
use View;

use App\SystemDetails;
use App\AccessMatrixDepartment;

class AccessMatrixDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = DB::table('system_details')
            ->leftJoin('access_matrix_department', 'system_details.id', '=', 'access_matrix_department.system_id')
            ->where('system_details.status', 'Active')
            ->get();


        //dd($users);


        //Distinct record access matrix departments
        $accessMatrix = DB::table('access_matrix_department')
                            ->orderBy('department_id', 'asc')
                            ->distinct()
                            ->get(['department_id']);

        //dd($accessMatrix[2]);

        //Get all record of access matrix department
        /*$accessMatrixAll = DB::table('system_details')
                            ->leftJoin('access_matrix_department', 'system_details.id', '=', 'access_matrix_department.system_id')
                            ->where('system_details.status', 'Active')
                            ->orderBy('department_id', 'asc')
                            ->get();*/
        //Get all record of access matrix department
        $accessMatrixAll = DB::table('system_details')
                            ->leftJoin('access_matrix_department', 'system_details.id', '=', 'access_matrix_department.system_id')
                            ->where('system_details.status', 'Active')
                            ->orderBy('department_id', 'asc')
                            ->orderBy('access_matrix_department.id', 'asc')
                            ->get();


        //Get all system details record
        $systemDetails = SystemDetails::where('status', 'Active')->get();   

        //Count Access Matrix
        $accessMatrixAllCount = DB::table('system_details')
                            ->leftJoin('access_matrix_department', 'system_details.id', '=', 'access_matrix_department.system_id')
                            ->where('system_details.status', 'Active')
                            ->count();




        //Count Sytem Details
        $systemDetailsCount = SystemDetails::where('status', 'Active')->count();

 

        return View::make('cms/access_matrix_department/index')
                        ->with(compact('total'))
                        ->with(compact('systemDetailsCount'))
                        ->with(compact('accessMatrixAllCount'))
                        ->with(compact('accessMatrix'))
                        ->with(compact('accessMatrixAll'))
                        ->with(compact('systemDetails'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Last id of Access Matrix
        $count = AccessMatrixDepartment::orderBy('id', 'desc')->first();

        //$users = collect($request->input());
        //dd($users);



        for($i = 1; $i <= $count->id; $i++){

            $accessMatrix = AccessMatrixDepartment::where('id', '=', $i)
                                        ->first();


            if($accessMatrix == null){
                continue;
            }


            
            if($request->input('hris'.$i)){
                $accessMatrix->hasAccess = 1;

            }

            else{
                $accessMatrix->hasAccess = 0;
            }


            $accessMatrix->save();

        }
        return redirect('access_matrix_department')->with('matrixSuccess', 'Matrix Access has been updated.');
        


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
