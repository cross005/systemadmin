<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


use DB;
use View;
use Response;
use Mail;

use Carbon\Carbon;

use App\User;
use App\SystemAdminUsers;
use App\Jobs\SendEmailQueued;
use App\Jobs\SendPasswordJob;

class PagesController extends Controller
{
    //User account is deactivated
    public function not_authorized(){
        return view('errors/not_authorized');
    }

    //User account is deactivated
    public function no_department(){
        return view('errors/no_department');
    }


    //Update record of users
    //Update record of users
    public function createRecord(Request $request){

        $email = $request->input('email');
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr( str_shuffle( $chars ), 0, 8 );
        $passwordHash = bcrypt($password);
        $users = SystemAdminUsers::where('email', $email)
                        ->first();

        if($users == null){
            return redirect('verifyLogin')->with('error', 'Account not found.');
        }

        //If user have no password
        if($users->isDeleted == 0){
            if($users->email == $email){

                if($users->password == null || $users->password == ""){
                        SystemAdminUsers::where('email', $email)
                                    ->update(['password' => $passwordHash]);

                        $receiverAddress = $email;
                        $content = [
                        'title'=> 'User Credentials',
                        'body'=> 'Email: '.$email,
                        'details' => 'Password: '.$password,
                        'button' => 'Click Here'
                        ];

                        $job = (new SendPasswordJob($content, $receiverAddress))
                            ->delay(Carbon::now()->addSeconds(1));

                        dispatch($job);
                        return redirect('login')->with('success', 'Password has been set to your email!');
                    }


                    else{
                        return redirect('login')->with('success', 'Welcome to Solar Philippines!');
                    }
            }
        }
        if($users->isDeleted == 1){
            return view('auth.login');
        }

    }
}
