<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

use Yajra\DataTables\Facades\DataTables;

use Response;
use Session;
use DB;
use View;


use App\HRISEmployeeRecord;
use App\SystemDetails;
use App\AccessMatrixDepartment;
use App\DepartmentDetails;

class SystemDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //return view('system_details/index');
        //$someModel = new HRISEmployeeRecord;
        //$someModel->setConnection('mysql2');

        //$something = $someModel->find(113);


        //
        /*$itemDetailsRecords = HRISEmployeeRecord::where('id', 113)
                                            ->where('show', 1)
                                            ->get();

        dd($itemDetailsRecords);

        $users = DB::connection('mysql2')->select('select * from user');
        dd($users);
        $testuser = TestUser::orderBy('id', 'desc')->first();
        dd($testuser);


        dd(123);*/
        return view('/cms/system_details/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cms/system_details/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {





        //Check if field is null
        $this->validate($request, [
            'system_name' => 'required',
            'url' => 'required'
        ]);

        $system_code = strtolower($request->input('system_name'));
        $system_code1 = str_replace(' ', '_', $system_code);

        $systemDetails = new SystemDetails;
        $systemDetails->system_code = $system_code1;
        $systemDetails->name = $request->input('system_name');
        $systemDetails->url = $request->input('url');
        $systemDetails->status = 'Active';
        $systemDetails->save();


        //Select last if of system details
        $systemDetailsLastId = SystemDetails::orderBy('id', 'desc')->first();

        //Get department id
        $departmentId = DepartmentDetails::orderBy('id', 'asc')->get();

        //Get last system_id of department_id
        $systemId = AccessMatrixDepartment::orderBy('id', 'desc')->first();



        foreach ($departmentId as $departmentIds) {
            $accessMatrixDepartment = new AccessMatrixDepartment;
            $accessMatrixDepartment->department_id = $departmentIds->department_id;
            $accessMatrixDepartment->system_id = $systemDetailsLastId->id;
            $accessMatrixDepartment->hasAccess = '0';
            $accessMatrixDepartment->save();
        }
        

        return redirect('system_details/')->with('success', 'System Name Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('/cms/system_details/show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $systemDetails = SystemDetails::find($id);

        return View::make('/cms/system_details/edit')
                ->with(compact('systemDetails'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getSystemDetailsDatatables(){
        /*return DataTables::eloquent(HRISEmployeeRecord::query()
                            ->where('show', 1))
                            ->make(true);*/
        return DataTables::eloquent(SystemDetails::query())
                            ->make(true);
    }


    //Deactivate System Details
    public function deactivateSystemDetails($id){
        $systemDetails = SystemDetails::findOrFail($id);

        $systemDetails->status = 'Deactivate';
        $systemDetails->save();
    }

    //Activate Account
    public function activateSystemDetails($id){
        $systemDetails = SystemDetails::findOrFail($id);

        $systemDetails->status = 'Active';
        $systemDetails->save();
    }
}
