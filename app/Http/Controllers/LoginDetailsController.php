<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

use Yajra\DataTables\Facades\DataTables;

use Response;
use DB;

use App\User;
use App\EmployeeDetails;

class LoginDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('/cms/login_details/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$permission = Permission::create(['name' => 'edit articles']);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getLoginDetailsDatatables(){
        /*return DataTables::eloquent(HRISEmployeeRecord::query()
                            ->where('show', 1))
                            ->make(true);*/
        return DataTables::eloquent(User::query())
                            ->make(true);
    }


    //Deactivate Account
    public function deactivateAccount($id){
        $users = User::findOrFail($id);

        $users->status = 'Deactivate';
        $users->save();
    }

    //Activate Account
    public function activateAccount($id){
        $users = User::findOrFail($id);

        $users->status = 'Active';
        $users->save();
    }
}
