<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\EmployeeDetails;
use App\AccessMatrix;
use App\AccessMatrixDepartment;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $employeeId = $user->employee_id;
        $empDetails = EmployeeDetails::where('employee_id', $employeeId)
                        ->first();

        if($empDetails == null){
            Auth::logout();

            return view('errors/no_department');
        }

        $departmentId = $empDetails->department_id;



        //User access rights
        $accessMatrixDepartment = AccessMatrixDepartment::where('department_id', $departmentId)
                                                        ->where('system_id', 4)
                                                        ->first();
        


        if($accessMatrixDepartment->hasAccess == 0){
            Auth::logout();
            return view('errors/no_department');
        }


        if($accessMatrixDepartment->hasAccess == 1){
            return view('/home');
        }

        else{
            Auth::logout();
            return view('errors/not_authorized');
        }

        return view('home');
    }
}
