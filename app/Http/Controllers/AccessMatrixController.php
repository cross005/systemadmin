<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

use Yajra\DataTables\Facades\DataTables;

use Response;
use Session;
use DB;
use View;


use App\User;
use App\SystemDetails;
use App\AccessMatrix;
use App\DepartmentDetails;

class AccessMatrixController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        /*$insertMatrix = DepartmentDetails::get();

        for($i = 0; $i < 2; $i++){


            //dd($insertMatrix[$i]->department_id);
            $multipleAccessMatrix = new AccessMatrix;

            if($insertMatrix[$i]->department_id == $multipleAccessMatrix[$i]->department_code){
                dd(123);
                return view(123);
            }

            else{
                $multipleAccessMatrix->department_code = $insertMatrix[$i]->department_id;
                $multipleAccessMatrix->hris = '0';
                $multipleAccessMatrix->service_desk = '0';
                $multipleAccessMatrix->purchase_order = '0';
                $multipleAccessMatrix->system_admin = '0';
                $multipleAccessMatrix->save();
            }
        }*/



        $accessMatrix = AccessMatrix::get();

        $systemDetail = SystemDetails::where('status', 'Active')
                                ->get();

        //$employeeRecord = User::where('status', 'Active')
        //                        ->get();
        $employeeRecord = User::join('tjsg_hris.employee_details', 'users.employee_id', '=', 'tjsg_hris.employee_details.employee_id')
                                //->join('access_matrix', 'users.employee_id', '=', 'access_matrix.employee_id')
                                ->get();


        //dd($employeeRecord);

        //$accessMatrix = User::join('access_matrix', 'users.employee_id', '=', 'access_matrix.employee_id')
        //                       ->get();




        //$employeeRecord = User::join('tjsg_hris.employee_details', 'users.employee_id', '=', 'tjsg_hris.employee_details.employee_id')
        //                    ->join('access_matrix', 'users.employee_id', '=', 'access_matrix.employee_id')

        //    ->select(['users.employee_id as employee_id', 'tjsg_hris.employe e_details.department_id', 'tjsg_hris.employee_details.lastname', 'tjsg_hris.employee_details.firstname', 'access_matrix.hasAccess']);

        //dd($employeeRecord);


        return View::make('cms/access_matrix/index')
                    ->with(compact('accessMatrix'))
                    ->with(compact('systemDetail'))
                    ->with(compact('employeeRecord'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $count = AccessMatrix::count();

        for($i = 1; $i <= $count; $i++){

            $accessMatrix = AccessMatrix::where('id', '=', $i)->first();

            if($request->input('hris'.$i)){
                $accessMatrix->hris = 1;
            }
            else{
                $accessMatrix->hris = 0;
            }

            if($request->input('service_desk'.$i)){
                $accessMatrix->service_desk = 1;
            }
            else{
                $accessMatrix->service_desk = 0;
            }

            if($request->input('purchase_order'.$i)){
                $accessMatrix->purchase_order = 1;
            }
            else{
                $accessMatrix->purchase_order = 0;
            }

            if($request->input('system_admin'.$i)){
                $accessMatrix->system_admin = 1;
            }
            else{
                $accessMatrix->system_admin = 0;
            }
            $accessMatrix->save();


        }
        return redirect('/access_matrix/')->with('matrixSuccess', 'Matrix Access has been updated.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function getEmployeeRecord(){


    }

    public function getAccessMatrixDatatables(){
        /*return DataTables::eloquent(HRISEmployeeRecord::query()
                            ->where('show', 1))
                            ->make(true);*/


        //Access Matrix with datatables
        /*
        $loginDetails = User::join('tjsg_hris.employee_details', 'users.employee_id', '=', 'tjsg_hris.employee_details.employee_id')
                            ->join('access_matrix', 'users.employee_id', '=', 'access_matrix.employee_id')

            ->select(['users.employee_id as employee_id', 'tjsg_hris.employee_details.department_id', 'tjsg_hris.employee_details.lastname', 'tjsg_hris.employee_details.firstname', 'access_matrix.hasAccess']);

        return DataTables::of($loginDetails)

                        //->addColumn('link', '<input type="checkbox" name="selected_users[]" value="{{ $id }}">')
                        ->addColumn('link', '<a href="{{ $department_id  }}">{{ $hasAccess }}</a>')
                        ->escapeColumns(['link1'])
                        ->make(true);
        */

        /*
        return DataTables::of($loginDetails)
                        ->addColumn('link', '<input type="checkbox" />')
                        ->escapeColumns(['link1'])
                        ->make(true);
        */
    }
}
