<?php

namespace App\Http\Controllers;

use App\DepartmentDetails;
use Illuminate\Http\Request;

class DepartmentDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DepartmentDetails  $departmentDetails
     * @return \Illuminate\Http\Response
     */
    public function show(DepartmentDetails $departmentDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DepartmentDetails  $departmentDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(DepartmentDetails $departmentDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DepartmentDetails  $departmentDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DepartmentDetails $departmentDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DepartmentDetails  $departmentDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(DepartmentDetails $departmentDetails)
    {
        //
    }
}
