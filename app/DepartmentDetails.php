<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentDetails extends Model
{
    //Table Name
    protected $table = 'department_details';
    //Primary Key
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;

    protected $connection = 'mysql2';
}
