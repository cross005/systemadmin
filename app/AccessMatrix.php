<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessMatrix extends Model
{
    //Table Name
    protected $table = 'access_matrix';
    //Primary Key
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;
}
