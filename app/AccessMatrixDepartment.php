<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessMatrixDepartment extends Model
{
    //Table Name
    protected $table = 'access_matrix_department';
    //Primary Key
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;

    protected $fillable = ['hasAccess', 'id', 'department_id'];
}
