<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemDetails extends Model
{
    //Table Name
    protected $table = 'system_details';
    //Primary Key
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;
}
