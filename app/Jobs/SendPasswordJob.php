<?php

namespace App\Jobs;

use App\Mail\SendPasswordMailable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Http\Request;
use Mail;

class SendPasswordJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $content;
    protected $receiverAddress;
    public function __construct($content, $receiverAddress)
    {
        $this->content = $content;
        $this->receiverAddress = $receiverAddress;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->receiverAddress)
            ->send(new SendPasswordMailable($this->content));
    }
}
